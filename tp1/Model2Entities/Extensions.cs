﻿using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Model2Entities
{
    public static class Extensions  
    {
        public static Book ConvertToModel(this BookEntity book)
        {
            return new Book(book.Id, book.Title, book.Author, book.Isbn);
        }

        public static BookEntity  ConvertToEntity(this Book book)
        {
            return new BookEntity() {Id= book.Id, Title = book.Title, Author = book.Author, Isbn = book.Isbn };
        }
    }
}
