﻿using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using Model;
namespace Model2Entities
{
    public class DbDataManager : IDataManager
    {
        public void CreateBook(Book book) {
            using (var context = new LibraryContext())
            {
                context.Database.EnsureCreated();
                if (!context.BooksSet.Contains(book.ConvertToEntity())){
                    context.BooksSet.Add(book.ConvertToEntity());
                }
                context.SaveChanges();
            }
        }


        public List<Book> GetAllBooks()
        {

            List<Book> books = new List<Book>();
            using (var context = new LibraryContext())
            {
                foreach (var bookEntity in context.BooksSet) {

                    books.Add(bookEntity.ConvertToModel());
                }

                return books;
            }
        }
        public Book GetBookById(long id)
        {
          
            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Id == id)
                    {
                        return i.ConvertToModel();
                    }
                }
                return null;
            }
        }

        public Book GetBookByAuthor(string author)
        {

            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Author == author)
                    {
                        return i.ConvertToModel();
                    }
                }
                return null;
            }
        }

        public Book GetBookByTitle(string title)
        {

            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Title == title)
                    {
                        return i.ConvertToModel();
                    }
                }
                return null;
            }
        }

        public Book GetBookByIsbn(string isbn)
        {

            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Isbn == isbn)
                    {
                        return i.ConvertToModel();
                    }
                }
                return null;
            }
        }
        public void UpdateAuthor(long id, string author)
        {
            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Id == id)
                    {
                        i.Author = author;
                    }
                }
                context.SaveChanges();
            }
        }

        public void DeleteBook(long id)
        {
            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    if (i.Id == id)
                    {
                        context.Remove(i);
                    }
                    
                }
                context.SaveChanges();
            }
        }
        public void DeleteAll()
        {
            using (var context = new LibraryContext())
            {
                foreach (var i in context.BooksSet)
                {
                    context.Remove(i);
                }
                context.SaveChanges();
            }
        }
    }
}
