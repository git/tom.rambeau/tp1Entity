﻿using Model;
using Entities;
using Model2Entities;
using Microsoft.Extensions.DependencyModel;
using DbContextLib;

// See https://aka.ms/new-console-template for more information



Console.WriteLine("Hello, World!");
DbDataManager dbDataManager = new DbDataManager();


    dbDataManager.CreateBook(new Book(1, "mistake", "test1", "test1"));
    dbDataManager.CreateBook(new Book(2, "the100", "test2", "test2"));
    dbDataManager.CreateBook(new Book(3, "GOT", "lastTest", "lastTest"));



Console.WriteLine("test de récupération de tout les livres");

List<Book> books = dbDataManager.GetAllBooks();
foreach (var book in books)
{
    Console.WriteLine("Titre du livre : " + book.Title);
}

Console.WriteLine("\ntest de récupération de livre par ID");
Book b1 = dbDataManager.GetBookById(3);
if (b1 != null)
{

    Console.WriteLine("Le livre : " + b1.Title);
}

Console.WriteLine("\ntest de récupération de livre par Autheur");
Book b2 = dbDataManager.GetBookByIsbn("test2");
Console.WriteLine("Le livre : " + b2.Title);


Console.WriteLine("\ntest de récupération de livre par Titre");
Book b3 = dbDataManager.GetBookByTitle("the100");
Console.WriteLine("Le livre : " + b3.Title);

Console.WriteLine("\ntest de récupération de livre par ISBn");
Book b4 = dbDataManager.GetBookByAuthor("lastTest");
Console.WriteLine("Le livre : " + b4.Title);

Console.WriteLine("\n MAJ de l'auteur du livre th100");
dbDataManager.UpdateAuthor(2, "Kass Morgan");
Book the100 = dbDataManager.GetBookByAuthor("Kass Morgan");
Console.WriteLine(the100.Title + " " + the100.Author);

Console.WriteLine("\nsupresion du livre ou id = 1");
dbDataManager.DeleteBook(1);


Book test = dbDataManager.GetBookById(1);
if (test == null)
{
    Console.WriteLine("livre supprimée");
}

Console.WriteLine("\nsuppression de tous les livres");




dbDataManager.DeleteAll();
