﻿using StubbedContextLib;
using Entities;
using DbContextLib;
using Microsoft.Extensions.Options;
using System.Reflection;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;

// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

using (var context = new StubbedContext())
{

    context.Database.EnsureCreated();

    PersonEntity p1 = new PersonEntity() { FirstName = "Tom", LastName = "Rambeau" };
    PersonEntity p2 = new PersonEntity() { FirstName = "Erwan", LastName = "Manager" };

    BookEntity chewie = new BookEntity() { Title = "mistake", Author = "test1", Isbn ="test1"};
    BookEntity the100 = new BookEntity() { Title = "the100", Author = "test4", Isbn = "test4"};
    BookEntity GOT = new BookEntity() { Title = "GOT", Author = "lastTest", Isbn = "lastTest"};

    context.Add(p1);
    context.Add(p2);
    context.Add(chewie);
    context.Add(GOT);
    context.Add(the100);

    context.SaveChanges();
}

using (var context = new StubbedContext())
{
    foreach (var n in context.BooksSet)
    {
        Console.WriteLine($"Books: {n.Id} - {n.Title}");
    }
    context.SaveChanges();
}


using (var context = new StubbedContext())
{
    var eBooks = context.BooksSet.Where(b => b.Title.StartsWith("t")).First();
    Console.WriteLine($"{eBooks.Title} (made by {eBooks.Author})");
    eBooks.Title = "Border";
    context.SaveChanges();
}

using (var context = new StubbedContext())
{
    Console.WriteLine("Deletes one item from de database");
    var impostor = context.BooksSet
        .SingleOrDefault(n => n.Title.Equals("mistake"));
    context.Remove(impostor);
    context.PersonSet.Include(pers => pers.Books).ToList();
    context.SaveChanges();

}

using (var context = new StubbedContext())
{
    Console.WriteLine("All people");
    var people = context.PersonSet;
    foreach(var p in people)
    {
        Console.WriteLine($"firstname: {p.FirstName}, lastname: {p.LastName}");   
    }
}

//emprunt
using (var context = new StubbedContext())
{
    var books = context.BooksSet;

    context.PersonSet.Include(pers => pers.Books).ToList();
    var person = context.PersonSet.Where(b => b.FirstName.StartsWith("E")).First();
    foreach (var book in books)
    {
        if (book.Owner == null)
        {
            person.Books.Add(book);
            break;
        }
    }
    context.SaveChanges();
}


//Rendu
using (var context = new StubbedContext())
{
    context.PersonSet.Include(pers => pers.Books).ToList();
    var books = context.BooksSet;
    if (books != null)
    { 
        foreach (var book in books)
        {
            //Console.WriteLine(book.Owner.FirstName);
            if (book.Owner != null)
            {
                Console.WriteLine($" \n propriétaire du livre {book.Title} avant rendu : {book.Owner.FirstName}");
                book.Owner = null;
                Console.WriteLine($" \n propriétaire a rendu  le libre {book.Title} ");
            }
            else
            {
                Console.WriteLine("\n livre sans propriétaire : " + book.Title);
            }
        }
    }
    
    context.SaveChanges();
}


//DeleteAllItem
using (var context = new StubbedContext())
{
    var allBooks = context.BooksSet;
    if ( allBooks != null)
    {
        foreach (var book in allBooks)
        {
            context.BooksSet.Remove(book);
        }
    }
    var allPeople = context.PersonSet;
    if (allBooks != null)
    {
        foreach (var person in allPeople)
        {
            context.PersonSet.Remove(person);
        }
    }
    context.SaveChanges();
}