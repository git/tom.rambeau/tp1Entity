﻿using System.Reflection;
using System.Text;

namespace Model
{
    public class Book
    {

        private long id;
        public long Id 
        {
            get { return id; }
            set { id = value;} 
        }

        private string title;
        public string Title 
        { 
            get { return title; }
            set { title = value; } 
        }

        private string author;
        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        private string isbn;
        public string Isbn {
            get { return isbn; }
            set { isbn = value; } 
        }
        public Book(long id, string title = "", string author = "", string isbn = "")
        {
            Id = id;
            Title = title;
            Author = author;
            Isbn = isbn;
        }

        public string Tostring(StringBuilder sb)
        {
            return Id.ToString() + " " + Title.ToString() + " " + Author.ToString() + " " + Isbn.ToString() ;
        }

    }



}
    