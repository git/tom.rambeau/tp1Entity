﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public interface IDataManager
    {
        //public IEnumerable<Book> GetBooks(int index, int count, BookOrderCriteria orderCriterium);
        //public IEnumerable<Book> GetBooksByTitle(string title, int index, int count, BookOrderCriteria orderCriterium);
        //public IEnumerable<Book> GetBooksByAuthor(string author, int index, int count, BookOrderCriteria orderCriterium);
        //public IEnumerable<Book> GetBooksByIsbn(string isbn, int index, int count, BookOrderCriteria orderCriterium);
        public Book GetBookById(long id);

        //public void CreateBook(string title, string author, string isbn);
        //public void UpdateBook(long id, Book book);
        //public void DeleteBook(long id);
    }
}
