﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Entities;
using Microsoft.Extensions.Options;


namespace DbContextLib
{
    public class LibraryContext : DbContext
    {
        public DbSet<PersonEntity> PersonSet { get; set; }
        public DbSet<BookEntity> BooksSet { get; set; } 

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=tp.Books.db");
            }
        }

        public LibraryContext()
        { }
        public LibraryContext(DbContextOptions <LibraryContext> options )
            : base(options)
        { }

        public void AddBook(BookEntity book)
        {
            if (!BooksSet.Contains(book))
            {
                BooksSet.Add(book);
            }
        }

        public void AddPerson(PersonEntity person)
        {
            if (!PersonSet.Contains(person))
            {
                PersonSet.Add(person);
            }
        }
        public void RemoveAll()
        {

            if (BooksSet.Count() != 0)
            {
                foreach (var book in BooksSet)
                {
                    BooksSet.Remove(book);
                }
            }
            if (PersonSet.Count() != 0)
            {
                foreach (var person in PersonSet)
                {
                    PersonSet.Remove(person);
                }
            }

        }

    }
}
