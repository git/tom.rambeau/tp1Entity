using DbContextLib;
using StubbedContextLib;
using Entities;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;
using Microsoft.Data.Sqlite;
using System;
using static System.Reflection.Metadata.BlobBuilder;

namespace UnitTests
{
    public class UnitTest1
    {

        [Fact]
        public void Add_TestBooks()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                context.Database.EnsureCreated();
                context.PersonSet.Include(pers => pers.Books).ToList();
                BookEntity the100 = new BookEntity { Title = "the100", Author = "", Isbn = "", Id =3 };
                BookEntity princeOfPersia = new BookEntity { Title = "princeOfPersia", Author = "", Isbn = "", Id = 4 };
                BookEntity PercyJackson = new BookEntity { Title = "PercyJackson", Author = "", Isbn = "", Id = 5 };
                context.AddBook(the100);
                context.AddBook(princeOfPersia);
                context.AddBook(PercyJackson);
                context.SaveChanges();
            }


            //uses another instance of the context to do the tests
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext(options))
            {
                context.Database.EnsureCreated();
                Assert.Equal(3, context.BooksSet.Count());
                Assert.Equal(1, context.BooksSet.Where(b => b.Title.Contains("the100")).Count());
                context.RemoveAll();
                context.SaveChanges();
            }
        }

        [Fact]
        public void Add_TestPersons()
        {
            //connection must be opened to use In-memory database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                //context.Database.OpenConnection();
                context.Database.EnsureCreated();
                context.PersonSet.Include(pers => pers.Books).ToList();
                PersonEntity p1 = new PersonEntity { FirstName = "Franc", LastName = "Bertinelli", Id = 2 };
                PersonEntity p2 = new PersonEntity { FirstName = "Jean", LastName = "Dubois", Id = 3 };
                context.AddPerson(p1);
                context.AddPerson(p2);
                context.SaveChanges();
            }


            //uses another instance of the context to do the tests
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                context.Database.EnsureCreated();
                Assert.Equal(2, context.PersonSet.Count());
                Assert.Equal(1, context.PersonSet.Where(p => p.FirstName.Contains("Jean")).Count());
                context.RemoveAll();
                context.SaveChanges();
            }
        }


        [Fact]
        public void Modify_TestBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context

            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
               context.Database.EnsureCreated();
                context.PersonSet.Include(pers => pers.Books).ToList();
                BookEntity the100 = new BookEntity { Title = "the100", Author = "", Isbn = "", Id = 3 };
                BookEntity princeOfPersia = new BookEntity { Title = "princeOfPersia", Author = "", Isbn = "", Id = 4 };
                BookEntity PercyJackson = new BookEntity { Title = "PercyJackson", Author = "", Isbn = "", Id = 5 };
                context.AddBook(the100);
                context.AddBook(princeOfPersia);
                context.AddBook(PercyJackson);
                context.SaveChanges();
              
            }
            
            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                context.Database.EnsureCreated();
                context.PersonSet.Include(pers => pers.Books).ToList();
                var book = context.BooksSet.Where(n => n.Title.Contains("princeOfPersia")).FirstOrDefault();
                book.Title = "l'Odyss�e"; 
                Assert.Equal("l'Odyss�e", book.Title);
                context.RemoveAll();
                context.SaveChanges();
            }
        }
        [Fact]
        public void Modify_TestPerson()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context

            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {

                context.PersonSet.Include(pers => pers.Books).ToList();
                PersonEntity p1 = new PersonEntity { FirstName = "Franc", LastName = "Bertinelli", Id = 2 };
                PersonEntity p2 = new PersonEntity { FirstName = "Jean", LastName = "Dubois", Id = 3 };
                context.AddPerson(p1);
                context.AddPerson(p2);
                context.SaveChanges();
            }

            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                context.PersonSet.Include(pers => pers.Books).ToList();
                string nameToFind = "Jean";
                Assert.Equal(1, context.PersonSet.Where(p => p.FirstName.Contains(nameToFind)).Count());
                var person = context.PersonSet.Where(p => p.FirstName.Contains(nameToFind)).First();
                person.FirstName = "Jacques";

                Assert.Equal("Jacques", person.FirstName);

                context.RemoveAll();
                context.SaveChanges();
            }
        }

        [Fact]
        public void Delete_TestPerson()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                PersonEntity p1 = new PersonEntity { FirstName = "Franc", LastName = "Bertinelli", Id = 2 };
                PersonEntity p2 = new PersonEntity { FirstName = "Jean", LastName = "Dubois", Id = 3 };
                context.AddPerson(p1);
                context.AddPerson(p2);
                context.SaveChanges();
            }
            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {

                context.PersonSet.Include(pers => pers.Books).ToList();
                var Persons  = context.PersonSet;
                if (Persons.Count() != 0)
                {
                    foreach (var person in Persons)
                    {
                        context.PersonSet.Remove(person);
                    }
                }
                context.SaveChanges();
                Assert.Equal( 0, context.PersonSet.Count());
                context.RemoveAll();
                context.SaveChanges();
            }
        }

        [Fact]
        public void Delete_TestBook()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            using (var context = new StubbedContext())
            {
                BookEntity the100 = new BookEntity { Title = "the100", Author = "", Isbn = "", Id = 3 };
                BookEntity princeOfPersia = new BookEntity { Title = "princeOfPersia", Author = "", Isbn = "", Id = 4 };
                BookEntity PercyJackson = new BookEntity { Title = "PercyJackson", Author = "", Isbn = "", Id = 5 };
                context.AddBook(the100);
                context.AddBook(princeOfPersia);
                context.AddBook(PercyJackson);
                context.SaveChanges();
            }

            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {   
                context.PersonSet.Include(pers => pers.Books).ToList();
                 var Books = context.BooksSet;
                if (Books.Count() != 0)
                {
                    foreach (var book in Books)
                    {
                        context.BooksSet.Remove(book);
                    }
                }
                context.SaveChanges();
                Assert.Equal(0, context.BooksSet.Count());
                context.RemoveAll();
                context.SaveChanges();

            }
        }


        [Fact]
        public void Update_TestEmprunt()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            using (var context = new StubbedContext())
            {

                context.PersonSet.Include(pers => pers.Books).ToList();
                PersonEntity p1 = new PersonEntity { FirstName = "Franc", LastName = "Bertinelli", Id = 2 };
                PersonEntity p2 = new PersonEntity { FirstName = "Jean", LastName = "Dubois", Id = 3 };
                context.AddPerson(p1);
                context.AddPerson(p2);
                BookEntity the100 = new BookEntity { Title = "the100", Author = "", Isbn = "", Id = 3 };
                BookEntity princeOfPersia = new BookEntity { Title = "princeOfPersia", Author = "", Isbn = "", Id = 4 };
                BookEntity PercyJackson = new BookEntity { Title = "PercyJackson", Author = "", Isbn = "", Id = 5 };
                context.AddBook(the100);
                context.AddBook(princeOfPersia);
                context.AddBook(PercyJackson);
                context.SaveChanges();

            }

            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                var books = context.BooksSet;

                context.PersonSet.Include(pers => pers.Books).ToList();
                var person = context.PersonSet.Where(b => b.FirstName.Contains("Jean")).First();
                foreach (var book in books)
                {
                    if (book.Owner == null)
                    {
                        book.Owner = person;
                        break;
                    }
                }

                context.SaveChanges();
            }

            using (var context = new StubbedContext())
            {
                Assert.NotNull(context.BooksSet.Where(b => b.Owner.FirstName.Contains("Jean")).First());
                context.RemoveAll();
                context.SaveChanges();
            }
        }

        [Fact]
        public void Update_TestRendu()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LibraryContext>().UseSqlite().Options;

            //prepares the database with one instance of the context
            using (var context = new StubbedContext())
            {

                context.PersonSet.Include(pers => pers.Books).ToList();
                PersonEntity p1 = new PersonEntity { FirstName = "Franc", LastName = "Bertinelli", Id = 2 };
                PersonEntity p2 = new PersonEntity { FirstName = "Jean", LastName = "Dubois", Id = 3 };
                context.AddPerson(p1);
                context.AddPerson(p2);
                BookEntity the100 = new BookEntity { Title = "the100", Author = "", Isbn = "", Id = 3 };
                BookEntity princeOfPersia = new BookEntity { Title = "princeOfPersia", Author = "", Isbn = "", Id = 4 };
                BookEntity PercyJackson = new BookEntity { Title = "PercyJackson", Author = "", Isbn = "", Id = 5 };
                context.AddBook(the100);
                context.AddBook(princeOfPersia);
                context.AddBook(PercyJackson);
                context.SaveChanges();

            }

            //using (var context = new StubbedContext(options))
            using (var context = new StubbedContext())
            {
                var books = context.BooksSet;

                context.PersonSet.Include(pers => pers.Books).ToList();
                var person = context.PersonSet.Where(b => b.FirstName.StartsWith("Jean")).First();
                var testbook = new BookEntity();
                foreach (var book in books)
                {
                    if (book.Owner == null)
                    {
                        book.Owner = person;
                        break;
                    }
                }

                foreach (var book in books)
                {
                    //Console.WriteLine(book.Owner.FirstName);
                    if (book.Owner != null)
                    {
                        book.Owner = null;  
                    }
                }
                Assert.Equal(context.BooksSet.Count(), context.BooksSet.Where(b => b.Owner == null).Count());
                context.RemoveAll();
                context.SaveChanges();
            }
        }

    }
}