﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StubbedContextLib.Migrations
{
    /// <inheritdoc />
    public partial class Mg : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PersonSet",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FirstName = table.Column<string>(type: "TEXT", nullable: false),
                    LastName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonSet", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BooksSet",
                columns: table => new
                {
                    Id = table.Column<long>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Title = table.Column<string>(type: "TEXT", nullable: false),
                    Author = table.Column<string>(type: "TEXT", nullable: false),
                    Isbn = table.Column<string>(type: "TEXT", nullable: false),
                    PersonId = table.Column<int>(type: "INTEGER", nullable: false),
                    OwnerId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BooksSet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BooksSet_PersonSet_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "PersonSet",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "BooksSet",
                columns: new[] { "Id", "Author", "Isbn", "OwnerId", "PersonId", "Title" },
                values: new object[,]
                {
                    { 1L, "test", "test", null, 1, "test" },
                    { 2L, "test2", "test2", null, 1, "test2" }
                });

            migrationBuilder.InsertData(
                table: "PersonSet",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { 1, "coco", "test" });

            migrationBuilder.CreateIndex(
                name: "IX_BooksSet_OwnerId",
                table: "BooksSet",
                column: "OwnerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BooksSet");

            migrationBuilder.DropTable(
                name: "PersonSet");
        }
    }
}
