﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib
{
    public class StubbedContext : LibraryContext
    {
        public StubbedContext() {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<PersonEntity>().HasData(
                new PersonEntity() { FirstName = "coco", LastName = "test", Id = 1 }
            );

            modelBuilder.Entity<BookEntity>().HasData(
               new BookEntity() { Title = "test", Author = "test", Isbn = "test" , Id = 1, PersonId = 1},
               new BookEntity() { Title = "test2", Author = "test2", Isbn = "test2",Id = 2, PersonId = 1 }
            );

            
        }
       
        public StubbedContext(DbContextOptions<LibraryContext> options)
            : base(options)
        { }

    }
}
